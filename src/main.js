import environment from './environment';
import {PLATFORM} from 'aurelia-pal';
import {Router} from 'aurelia-router';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .plugin('aurelia-animator-css')
		.plugin('aurelia-dialog', config => {
			config.useDefaults();
			config.settings.lock = true;
			config.settings.centerHorizontalOnly = false;
			config.settings.startingZIndex = 5;
		})
    .feature('resources');

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  return aurelia.start().then(() => aurelia.setRoot('anon'));
}
