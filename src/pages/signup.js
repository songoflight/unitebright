import { inject } from 'aurelia-framework';
import { avm } from '../components/data/avm';
@inject(avm)
export class SignUp {     
	constructor(avm) {
		this.avm = avm;
		this.title = "Sign Up";
	}
	activate() {
		this.avm.mvcMap(this, true);
	}
	
	clickDeli(ev) {
	// PURPOSE: This is where click events come from the view's click delegate.  
	// ctx{} should contain everything you need to know about 'what' was clicked.
		var ctx = this.avm.tools.eventContext(ev.target);
		if (ctx === false) { return true; }
		switch (ctx.cat) {
			case 'template':
			break;
		}
	}

	dbCb(msg) {
		var model = this.model;
		var avm = this.model.avm;
		var data = this.model.data;
		avm.session.fetching--;
		if (msg != null && !msg.error && msg.context) {
			avm.session.error = false;
			switch(msg.context) {
				case 'template':
					console.log(msg);
				break;
			}
		}
		else if (msg != null && msg.error && msg.errorMessage != '') {
			avm.session.error = msg.errorMessage;
		}
	}

	attached() {
	}

	detached() {
	}
}