import { inject } from 'aurelia-framework';
import { avm } from '../data/avm';
@inject(avm)
export class themes {
	constructor(avm) {
		this.avm = avm;
	}

	themeDefs = [
		{ name: 'light', label: 'Light',
			primary: {text: '#000', med: '#fff8e1', light: '#ffffff', dark: '#ccc5af', feat: '#ffecb3'},
			accent: {text: '#fff', med: '#3e2723', light: '#9a7f7b', dark: '#1b0000', feat: '#dc4930'},
			util: {text: '#fff', warn: '#f57c00', good: '#689f38', bad: '#955'}
		},
		{ name: 'dark', label: 'Dark',
			primary: {text: '#fff', med: '#3e2723', light: '#1b0000', dark: '#6a4f4b', feat: '#ac1900'},
			accent: {text: '#000', med: '#fff8e1', light: '#ffffff', dark: '#5c553f', feat: '#fbc02d'},
			util: {text: '#fff', warn: '#f57c00', good: '#689f38', bad: '#955'}
		},
	]

	setTheme(id, saveToDb) {
		if (id == null && this.active != null) { var id = this.active.id; }
		if (id == null) { id = 0; }
		this.active = this.themeDefs[id]
		this.active.id = id;
		this.genRules();
		this.setRules();
		if (saveToDb === true && this.avm.session.user && this.avm.session.user.uid) {
			this.avm.session.user.themeId = id;
			console.log(this.avm.session.user);
			this.avm.db.newDb('users', this.avm.session.user.uid).set({themeId: id}, {merge:true});
		}
	}

	genRules() {
		this.ruleKeys = [];
		var targets = [
			{ class: 'color', style: 'color' },
			{ class: 'bg', style: 'background-color' },
			{ class: 'bor', style: 'border-color' }
		]
	// Automatic Theme Rules
		for (var i = targets.length - 1; i >= 0; i--) {
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-text', rule: targets[i].style+': '+this.active.primary.text+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-med', rule: targets[i].style+': '+this.active.primary.med+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-light', rule: targets[i].style+': '+this.active.primary.light+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-dark', rule: targets[i].style+': '+this.active.primary.dark+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-feat', rule: targets[i].style+': '+this.active.primary.feat+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-text', rule: targets[i].style+': '+this.active.accent.text+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-med', rule: targets[i].style+': '+this.active.accent.med+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-light', rule: targets[i].style+': '+this.active.accent.light+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-dark', rule: targets[i].style+': '+this.active.accent.dark+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-feat', rule: targets[i].style+': '+this.active.accent.feat+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-text', rule: targets[i].style+': '+this.active.util.text+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-warn', rule: targets[i].style+': '+this.active.util.warn+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-good', rule: targets[i].style+': '+this.active.util.good+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-bad', rule: targets[i].style+': '+this.active.util.bad+' !important;' });
		};
	// Manual push rules
		this.ruleKeys.push({ sel: 'mark', rule: 'background-color: '+this.active.accent.med+' !important;' });
		this.ruleKeys.push({ sel: 'mark', rule: 'color: '+this.active.accent.text+' !important;' });
	}	

	setRules() {
	// This function creates or replaces the CSS for the applicable theme
		var cssRuleCode = document.all ? 'rules' : 'cssRules';
		var sheetID = document.styleSheets.length - 1;
		var sheet = document.styleSheets[sheetID];
		var rules = sheet[cssRuleCode];
		for (var x = rules.length - 1; x >= 0; x--) {
		// Loop over each stylesheet rule (sr)
			var sr = rules[x];
			for (var y = this.ruleKeys.length - 1; y >= 0; y--) {
				var tr = this.ruleKeys[y];
				if (sr.selectorText == tr.sel) {
				// Update the rule
					sr.cssText = tr.rule;
				}
				else if (x == 0) {
					// Append the rule
					try { sheet.insertRule(tr.sel+' {'+tr.rule+'}', rules.length); } 
						catch(err) { try { sheet.addRule(tr.sel, tr.rule); } 
						catch(err) { console.log("Couldn't add style");} }
				}
			};
		};
	}

}