import { observable } from 'aurelia-framework';

export class avm {
	constructor() {
		this.session = {
		// SET SESSION DEFAULTS
			appLoading: true,
			loggedIn: null,
			user:  null,
			fetching: 0,
			focus: null,
			defaultPrefs: {
		  	general: { themeId: 0 }
		  }
		}
		this.data = {
		}
	}

	mvcMap(mvc, db) {
	// PURPOSE: Exec's pageLoad actions and sets the avm.active and (optional) db context for the active view/view-model
	// ARG mvc [REQ]: The 'this' reference referred from the active view-model
	// ARG db [OPT]: If true, the database fetchers and callbacks get set up and mapped
		if (mvc == null) { return false; }
		this.active = mvc;
		if (db === true) {
			// this.fetch = this.db.newDb(mvc.dbCb);
			this.fetch = {};
			this.fetch.model = mvc;
		}
	}
}