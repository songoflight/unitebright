import { inject} from 'aurelia-framework';
import { DialogService } from 'aurelia-dialog';
import { promptYesNo } from '../prompts/yesno';
import { avm } from '../data/avm';
// Aurelia Docs: https://aurelia.io/docs/plugins/dialog#introduction

/* README
	** PARENT VIEW-MODEL (APP.JS) Code Example **
		import { inject } from 'aurelia-framework';
		import { avm } from './components/data/avm';
		import { prompts } from './components/services/prompts';

		@inject(avm, prompts)
		export class App {
			constructor(avm, prompts) {
				this.avm = avm;
				this.avm.prompts = prompts;
			}
		}

	** VIEW-MODEL (JS) Code Example**
		activate() { this.message = "Click Me" }
		question() {
			var avm = this.avm;
			this.avm.prompts.yesno({
				body: "<h3>Did You Click Me?</h3>",
				onYes: function() { this.caller.message = "You clicked me!" },
				onNo: function() { this.caller.message = "OK, good to know, my bad." }
			})
		}

	** VIEW (HTML) Code Example** 
		<span click.trigger="question()">${message}</span>
*/


@inject(avm, DialogService)
export class prompts {
	constructor(avm, DialogService) {
		this.avm = avm;
		this.ds = DialogService;
	}

	configDefaults = {
		labels: {
			yes: 	"Yes",
			no: 	"No",
			cancel: "Cancel"
		}
	}

	yesno(evData) {
		if (evData == null || evData.body == null) { return false; }
		var data = {
			body: evData.body,
			options: this.configDefaults,
			vm: promptYesNo,
			caller: this.avm.active,
			onYes: function() { console.log('YES') },
			onNo: function() { console.log('Canceled') }
		}
		if (typeof evData.onYes == "function") { data.onYes = evData.onYes }
		if (typeof evData.onNo == "function") { data.onNo = evData.onNo }
		if (evData.caller != null) { data.caller = evData.caller }
		this.initPrompt(data);
	}

	initPrompt(data) {
		this.ds.open({ viewModel: data.vm, model: data, lock: false }).whenClosed(response => {
		  if (!response.wasCancelled) {
		  	data.onYes()
		  } else {
		  	data.onNo();
		  }
		});
	}
}

