import { inject, Aurelia } from 'aurelia-framework';
import { avm } from '../data/avm';
@inject(avm, Aurelia)
export class dbService {
	constructor(avm, aurelia) {
		this.avm = avm;
		this.aurelia = aurelia;
	}

/*
	CF AJAX PROXT FUNCTION MAPS
*/

	newDb(collection, docId) {
		let db = firebase.firestore();
		let data = db;
		if (collection) {
			let col = db.collection(collection);
			data = col;
		}
		if (docId) {
			data = data.doc(docId);
		}
		return data;
	}

	getFamilies(church) {
		if (church == null) { var church = '58dca7b0-2106-6f71-9f68-9c44f3ef70bb'; }
		var fams = [];
		this.newDb('families').where('church', '==', church).get().then(e => 
			{e.forEach(f => { 
				fams.push(f.data()) 
			}) 
		});
		return fams;
	}

	dbError(msg) {
		console.warn('DB Service Error | '+msg);
	}

/*
	USER MANAGEMENT FUNCTIONS
*/		

	logInGoogle() {
		const prov = new firebase.auth.GoogleAuthProvider();
		this.avm.session.appLoading = true;
		firebase.auth().signInWithPopup(prov)
			.then(r => {
				let user = {
					context: 'loginGoogle',
					data: r.user
				}
				this.avm.db.mapUser(user)
			}, this.avm)
	}

	logOut() {
		let session = this.avm.session;
		let router = this.avm.router;
		if (session.loggedIn === true && session.user) {
			let user = this.avm.session.user;
			switch (user.context) {
				case 'loginGoogle':
					this.avm.app.auth().signOut()
					console.log('Google Logging Out!');
				break;
			}
			session.loggedIn = false;
			session.user = null;
		// Reset back to the generic login app
			this.aurelia.setRoot('anon').then(() => {	this.avm.router.navigateToRoute('welcome'); });
			console.log('downgrading to ANON shell');
		}
	}

	mapUser(user) {
		console.log(user);
		this.avm.session.appLoading = true;
		if (user != null && user.context) {
			this.avm.session.error = false;
		// Set up the session.user base on login context rules for the auth provider
			switch(user.context) {
				case 'loginGoogle':
					console.log('Google Logging In');
					this.avm.session.loggedIn = true;
					let authData = {
						uid: user.data.uid,
						displayName: user.data.displayName,
						email: user.data.email,
						photo: user.data.photoURL,
						context: user.context
					}
				// Logged in, great! Go to the secured App
					this.aurelia.setRoot('app').then(() => {
						this.avm.router.navigateToRoute('dashboard'); 
						this.avm.session.appLoading = true;
					});
					console.log('upgrading to APP shell');
					this.fetchUser(authData);
				break;
			}
		}
	}

	fetchUser(authData) {
	// Create/Update user account entry
		authData.lastLogin = new Date();
		this.newDb('users', authData.uid).set(authData, {merge:true}).then(e => {
			this.newDb('users', authData.uid).get().then(doc => {
				if (doc.exists) {
					let userData = doc.data()
	  	  	if (userData.themeId) { this.avm.themes.setTheme(userData.themeId, false) }
	  	  	else { 
	  	  		userData.themeId = this.avm.themes.active.id;
	  	  		this.avm.themes.setTheme(userData.themeId, true);
	  	  	}
	        this.avm.session.user = userData;
	  	  }
			}).then(() => {this.avm.session.appLoading = false;});
		})
	}

	checkUser() {
	}

	reMapUser() {
	// This function reconnects users if their session becomes disconnected
		if (this.avm.session.user == null) {
			var authData = firebase.auth();
			var currUser = firebase.auth().currentUser;
			if (currUser && currUser.uid) {
				this.mapUser({context: 'loginGoogle', data: currUser});
			}
			else {
				this.avm.session.appLoading = false;
			}
		}
		else {
			this.avm.session.appLoading = false;
		}
	}

}