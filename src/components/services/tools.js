export class tools { 

	getUrlVars(href) {
		if (href == null || href == '') { var href = location.href; }
		if (/\?/g.test(href) == false ) { return {error: 'no vars in query string'}; };
		var vars = {};
		href = href.split('?')[1];
		href = href.split('&');
		for (var i = href.length - 1; i >= 0; i--) {
			var keyName = href[i].split('=')[0];
			var keyVal = href[i].split('=')[1];
			vars[keyName] = keyVal;
		};
		return vars;
	}

	eventContext(target) {
	// PURPOSE: Maps the event target's dataset, class, id, etc and returns it as an easy to parse object
	// Validation
		if (target == null) { return false; }
		var ancestor = target.closest('[data-cat]');
		if (ancestor != null && (target.dataset == null || target.dataset.cat == null)) {	target = ancestor; }
		if (target.dataset.cat == null) {return false; }
	// Define and map
		var obj = {};
		obj.cat = target.dataset.cat || '';
		// if (isNaN(obj.cat)) { obj.cat = obj.cat.toLowerCase(); }
		obj.val = target.dataset.val || '';
		// if (isNaN(obj.val)) { obj.cat = obj.cat.toLowerCase(); }
		obj.id = target.id || '';
		obj.class = target.className || '';
		obj.parent = target.parentElement;
		obj.children = target.children;
		for (var key in target.dataset) {
			obj[key] = target.dataset[key];
			// if (isNaN(obj[key])) { obj[key] = obj[key].toLowerCase(); }
		}
		return obj;
	}

	matchTest(needle, haystack) {
		if (needle == '' || haystack == '') { return false; }
		var match = new RegExp(needle.toLowerCase(), 'ig');
		if (match.test(haystack.toLowerCase())) { return true; }
		else { return false; }
	}

	reExec(needle, haystack) {
		if (needle == '' || haystack == '') { return false; }
		var match = new RegExp(needle, 'ig');
		var result = match.exec(haystack);
		return result;
	}

	remHTMLTags(str) {
		return str.replace(/(<([^>]+)>)/ig, '');
	}

	toHex(c) {
		var hex = c.toString(16);
		return hex.length == 1 ? "0" + hex : hex;
	}

	rgbToHex(r, g, b) {
		return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
	}

	getRandBlue(base, offset) {
		if (offset == null) { var offset = 100; }
		var color = '#'+base+this.toHex(Math.floor(Math.random()*(255 - parseInt(offset))) + parseInt(offset));
		return color;
	}

	genExcuse() {
		var message = '';
		var excuse = '';
		// MESSAGE ITEMS
		var exclaim = ['I\'m sorry...','Yeah,','Well...','Ummm...','You see...','Yikes,','Wow,','Sorry,','My Bad...','Epic fail!','It\'s no good,','Apologies,','Error:','It\'s not my fault,','That didn\'t work -','This is Embarrassing...','Impossible!','Help!'];
		var msg = ['I couldn\'t load that',	'this is missing', 'this is not finished', 'it\'s missing', 'it\'s not responding', 'they\'re all gone!', 'it can\'t be done', 'it\'s taking too long','it\'s not \'Done Done Done\'','it\'s broke','it no worky','the thing is missing','Random Error!','stuff is broken','this was a bad idea','where am I?!','what was I doing?','who are you?!','what am I doing here?!'];
		// EXCUSE ITEMS
		var preface = ['It\'s because','There can be only one explanation:','It all started when...','All I know is that','Clearly','Apparently','Obviously','Fault 1337!','the stack trace says:'];
		var noun = ['the magic box', 'a hungry hungry hippo', 'the magical unicorn', 'an adorable little animal', 'the code', 'the data', 'a critical table', 'the backups', 'a very important fish', 'the magical unicorn', 'a required potato', 'a school of fish', 'my lunch', 'my heart', 'my breakfast', 'my dinner', 'a powerful enemy', 'my family', 'your computer', 'your browser', 'your monitor', 'your mouse', 'your keyboard', 'the Easter Bunny', 'Santa Clause', 'a gang of angry elves', 'the space rangers', 'some socially malnourished nerds', 'an innocent-looking pie', 'a sentient reindeer', 'three scary Nuns', 'you', 'the Internet', 'the developer', 'the \'IT\' department', 'the missing body', 'a horrible pun', 'a terrible joke', 'an inconsiderately discarded coffee table', 'a hug-able tree', 'a stray robot', 'a sad little man', 'an indescribable fellow', 'a lonely planet', 'a ravenous paperweight', 'a basket of yummy things', 'a figurative fig', 'a very literal zebra', 'an honest lawyer', 'the last honest pizza', 'a sarcastic dolphin', 'an overconfident asparagus', 'Leeroy Jenkins', 'a meat dragon', 'Chuck Norris', 'Abe Lincoln'];
		var bridge = ['must have', 'recently', 'effortlessly', 'underhandedly', 'eventually', 'mistakenly', 'unintentionally', 'maliciously', 'ruefully', 'angrily', 'gleefully', 'easily', 'loudly', 'quietly', 'cleverly', 'foolishly', 'fiendishly'];
		var verb = ['stole', 'ate', 'absconded with', 'amused', 'misdirected', 'lied to', 'busted', 'destroyed', 'tainted', 'corrupted', 'painted', 'decorated', 're-engineered', 'incapacitated', 'defeated', 'jerry-rigged', 'lost', 'never found', 'brainwashed', 'misinterpreted', 'scared off', 'deleted', 'overtaxed', 'underestimated', 'infected', 'offended', 'hid', 'transformed', 'regurgitated'];
		// SENTANCE ASSEMBLY LOGIX
		var noun1 = noun[Math.floor(Math.random() * noun.length)];
		var noun2 = noun[Math.floor(Math.random() * noun.length)];
		while (noun1 == noun2) {
			noun2 = noun[Math.floor(Math.random() * noun.length)];
		}
		message += exclaim[Math.floor(Math.random() * exclaim.length)]+' ';
		message += msg[Math.floor(Math.random() * msg.length)];
		excuse += preface[Math.floor(Math.random() * preface.length)]+' ';
		excuse += noun1+' ';
		excuse += bridge[Math.floor(Math.random() * bridge.length)]+' ';
		excuse += verb[Math.floor(Math.random() * verb.length)]+' ';
		excuse += noun2+'!';
		excuse = excuse.replace('  ', '');
		return [message, excuse];
	}
}	