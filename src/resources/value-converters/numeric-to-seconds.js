export class ToSecondsValueConverter {
    toView(secs) {
		// PURPOSE: for numeric arg: 'secs' returns time format '00:00'
			var t = Math.floor(secs).toString(); 
			var hr  = Math.floor(secs / 3600);
			var min = Math.floor((secs - (hr * 3600))/60);
			var sec = Math.floor(secs - (hr * 3600) -  (min * 60));
			if (hr < 1) { hr = ''; }
			else { hr = hr+':'; }
			if (min < 10){ 
				min = "0" + min; 
			}
			if (sec < 10){ 
				sec  = "0" + sec;
			}
			return hr + min + ':' + sec;
    }
}


/**
 * Usage
 *
 * <require from="../resources/value-converters/numeric-to-seconds"></require>
 * <span>${myStringNumber | ToSeconds}</span>
 */
