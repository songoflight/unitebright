/*
 * String Highlight Value Converter
 * Allows for a regular expression to be used in the view to return
 * a string with <mark> tags encapsulating all patterns within a value
*/

export class StringHighlightValueConverter {
	toView(value, pattern) {
		if (!value || !pattern) { return value; }
		let rePatt = new RegExp('('+pattern.toLowerCase()+'+)', 'ig');
		return (typeof value === 'string') ? value.replace(rePatt, '<mark>$1</mark>') : null;
	}
}

/*
 * Usage
 * <require from="resources/value-converters/string-highlight"></require>
 * <h1 textContent.bind="someValue | stringHighlight:'needle'"></h1>
*/