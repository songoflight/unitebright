import { inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { CssAnimator } from 'aurelia-animator-css';
import { Router } from 'aurelia-router';
import { dbService } from './components/services/dbservice';
import { avm } from './components/data/avm';
import { themes } from './components/data/themes';
import { tools } from './components/services/tools';
import { prompts } from './components/services/prompts';
import fontawesome from 'fontawesome';

@inject(EventAggregator, Router, dbService, avm, themes, tools, CssAnimator, prompts)
export class App {
	constructor(EventAggregator, Router, dbService, avm, themes, tools, CssAnimator, prompts) {
		this.ea = EventAggregator;
		this.router = Router;
		this.avm = avm;

	// --- THIS IS **ONLY** FOR DEBUG - NO CHEATING, USE AVM{} SCOPE! ---
	// ------------------------------------------------------------------

		this.avm.db = dbService;
		this.avm.themes = themes;
		this.avm.tools = tools;
		this.avm.prompts = prompts;
		this.avm.animator = CssAnimator;
		this.avm.router = Router;
		this.avm.appData = {
			title: 	"UniteBright",
			logoIcon: '/assets/images/icons/favicon-64.png',
			logoImg: '/assets/images/icons/favicon-96.png',
			tagLine: "A Place for Churches to Build Together",
			version: 0.01
		}

	// --- THIS IS **ONLY** FOR DEBUG - NO CHEATING, USE AVM{} SCOPE! ---
		window.avm = this.avm;
	// ------------------------------------------------------------------

		this.fireBaseInit();
/*
	Subscribe to router navigation events
*/
		this.ea.subscribe('router:navigation:processing', e => {
		// NAVIGATION IS PROCESSING ...
			if (!e.instruction.router.isExplicitNavigationBack) {
			// Refresh the user's login state
				avm.db.checkUser()
			// Close the mobile menu
				this.router.mobileMenuOpen = false;
			// Make sure the AJAX placeholder doesn't get out of bounds
				if (this.avm.session.fetching < 0) { this.avm.session.fetching = 0; }
			}
		});
		this.ea.subscribe('router:navigation:complete', e => {
		// NAVIGATION IS COMPLETED!   Yay!
			// Scroll To Top
				document.body.scrollTop = document.documentElement.scrollTop = 0;
			// Run active view-models attached() function in case we are navving within a child view-model
				if (this.avm && this.avm.active && this.avm.active.attached) { this.avm.active.attached(); }
		});
	}


	activate() {
	}

	attached() {
		this.avm.themes.setTheme();
	}

	configureRouter(config, router) {
		config.title = 'UniteBright';
		config.options.pushState = true;
		config.options.root = '';
		config.map([
			{ 
				route: ['', 'welcome', 'index', 'dashboard'], 
				name: ['dashboard', 'welcome'],
				moduleId: './pages/welcome',      
				nav: true, 
				settings: {class: ""},
				title: 'Welcome'
			},
			{ 
				route: ['about'], 
				name: 'about',      
				moduleId: './pages/about',      
				nav: true, 
				settings: {class: ""},
				title: 'About'
			},
			{ 
				route: ['signup'], 
				name: 'signup',      
				moduleId: './pages/signup',      
				nav: true, 
				settings: {class: ""},
				title: 'Sign Up'
			},
			{ 
				route: ['trivia'], 
				name: 'trivia',      
				moduleId: './pages/trivia',      
				nav: false, 
				settings: {class: ""},
				title: 'Trivia'
			}
		]);
		config.mapUnknownRoutes((instruction) => { instruction.router.navigateToRoute('welcome') });
		// config.mapUnknownRoutes((instruction) => { return {route: 'welcome', moduleId: 'welcome'} });
		this.router = router;
		this.router.mobileMenuOpen = false;
		this.router.menuDeli = function(ev) {
			var target = ev.target;
			if (target.dataset == null || target.dataset.cat == null || target.dataset.val == null) {
				target = target.closest('[data-cat]');
			}
			if (target == null) {	return true; }
			switch (target.dataset.cat) {
				case 'link':
					var navRoute = 'welcome';
					if (target.dataset.val != '' && target.dataset.val != null) {
						navRoute = target.dataset.val;
					}
					this.avm.router.navigateToRoute(navRoute);
				break;
				case 'mobileMenu':
				if (target.dataset.val == 'toggle') {
					if (this.mobileMenuOpen == false) { this.mobileMenuOpen = true; }
					else { this.mobileMenuOpen = false;	}
				}
			}
		}
		router.avm = this.avm;
	}
	fireBaseInit() {
		// Initialize Firebase
    try {
			var config = {
				apiKey: "AIzaSyBNgY-nUn4fIP7pfpKDjWv-mEmevRVhWiU",
				authDomain: "unitebright-200917.firebaseapp.com",
				databaseURL: "https://unitebright-200917.firebaseio.com",
				projectId: "unitebright-200917",
				storageBucket: "unitebright-200917.appspot.com",
				messagingSenderId: "639556505564"
			};
			firebase.initializeApp(config);
      this.avm.app = firebase.app();
      let features = ['auth', 'database', 'firestore', 'functions', 'storage'].filter(feature => typeof this.avm.app[feature] === 'function');
      console.log(`Firebase SDK loaded with ${features.join(', ')} modules`);
      setTimeout(function() {
      	this.avm.db.reMapUser();
      }, 500);
    } catch (e) {
      console.error(e);
    }
	}
}
