import {Redirect} from 'aurelia-router';
import { inject } from 'aurelia-framework';
import { avm } from '../components/data/avm';
@inject(avm)

export class authorizestep {
	
	constructor(avm) {
		this.avm = avm;
	}

	run(navigationInstruction, next) {
		if (navigationInstruction.getAllInstructions().some(i => i.config.settings.auth)) {
		// User needs to be logged in, check login state
			if (this.avm.session.loggedIn === true) {
			// Go ahead sir, your page awaits you!
		 		return next();
			}
			// User is not logged in, send to home page
				this.avm.entryURL = navigationInstruction.config.name;
				return next.cancel(new Redirect(''));
		}
		else {
			return next();
		}
	}

}